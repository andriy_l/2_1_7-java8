package com.brainacad;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@FunctionalInterface
public interface Monday {

    String getString();

    public static void print(){
        System.out.println("Print");
    }

    default String getString2(){
        List<String> al = Arrays.asList("a", "b");
        al.forEach((s -> s.toUpperCase()));

        al.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                s.toUpperCase();
            }
        });
//        al.forEach(String::toUpperCase);


        return "hello";
    }
//    String toString();
//    int hashCode();

}

class Tuesday implements Monday{

    @Override
    public String getString() {
        return null;
    }
}
