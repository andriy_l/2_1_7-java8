package com.brainacad.java8;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, String> props = new HashMap<>();
        props.put("one","один");
        props.put("two","dva");
        props.put("three","try");

        props.keySet().stream().map(k -> props.get(k)).forEach(System.out::println);

        LocalDate date = LocalDate.parse("2018-04-30");

        date.plusDays(2);

        date.plusYears(3);

        System.out.println(date.getYear() + ""  + date.getMonth() + "" + date.getDayOfMonth());

        LocalDateTime t1 = LocalDateTime.of(2016, 8, 28, 5, 0);
        LocalDateTime t2 = LocalDateTime.of(2016, 8, 28, 9, 0);
        ZoneId z1 = ZoneId.of("GMT-04:00");
        ZoneId z2 = ZoneId.of("GMT-06:00");
        ZonedDateTime zdt1 = ZonedDateTime.of(t1, z1);
        ZonedDateTime zdt2 = ZonedDateTime.of(t2, z2);
        System.out.println(zdt1);
        System.out.println(zdt2);
        System.out.println(zdt1.isBefore(zdt2));
        long p = zdt1.until(zdt2, ChronoUnit.HOURS);
          System.out.println(p);
//          2016-08-28T05:00 GMT-04:00
//          2016-08-28T09:00 GMT-06:00
//        The first date/time is earlier.
//        The date/times are 6 hours apart.

    }
}
