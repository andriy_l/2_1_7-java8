package com.brainacad.oop.streams174;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = Stream.<Integer>iterate(10, i -> i+10).limit(10).map(x -> x/2).collect(Collectors.toList());
        System.out.println("List: " + list);

        System.out.println("\nSelected and sorted list:");
        List<String> list1 = Arrays.asList("bnmju", "2123", "fadse", "czczx", "bbvc", "ytre", "ajdshds", "wwqrq", "bvcxz");
        list1.stream().filter(s -> s.startsWith("b")).sorted().forEach(System.out::println);

        System.out.println();
        List<Person> persons = Arrays.asList( new Person("Ivan", 16, Sex.MAN), new Person("Peter", 23, Sex.MAN),
                new Person("Galyna", 22, Sex.WOMEN), new Person("Olexander", 69, Sex.MAN),
                new Person("Nina", 42, Sex.WOMEN), new Person("Igor", 26, Sex.MAN),
                new Person("Olena", 30, Sex.WOMEN), new Person("Maksym", 34, Sex.MAN));

        System.out.println("Military:");
        persons.stream().filter((p)-> p.getAge() >= 18 && p.getAge() < 27 && p.getGender() == Sex.MAN).
                forEach(System.out::println);

        System.out.print("\nThe average age of women: ");
        double averageWomen = persons.stream().filter((p) -> p.getGender() == Sex.MAN).
                mapToInt(Person::getAge).average().getAsDouble();
        System.out.println(averageWomen);
    }
}
