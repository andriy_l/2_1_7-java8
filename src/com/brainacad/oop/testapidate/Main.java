package com.brainacad.oop.testapidate;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;

/**
 * Created by andriy on 03.04.17.
 */
public class Main {
    public static void main(String[] args) {
        // date
        LocalDate today = LocalDate.now();
        System.out.println("Now: " + today);
        LocalDate birtday = LocalDate.of(1990, Month.MAY, 10);
        System.out.println("Birthdate: " + birtday);
        int myYears = today.getYear() - birtday.getYear();
        int myMonths = today.getDayOfMonth() - birtday.getDayOfMonth();
        System.out.println("I have " + myYears);
        System.out.println("This year my birthday is in: " + birtday.plusYears(myYears).getDayOfWeek());
        // time
        LocalTime current = LocalTime.now();
        System.out.println(current);
        System.out.println(current.truncatedTo(ChronoUnit.SECONDS));

        // difference correct
        Period period = birtday.until(today);
        int years = period.getYears();
        System.out.println("correctly. NOw i'm " + years + " years old");
        LocalDate currentYearsBirthday = birtday.plusYears(years);
        period = Period.between(today, currentYearsBirthday);
        if(period.isNegative()){
            System.out.println("Birthday has already passed");
        } else {
            System.out.println("Birthday has not yet come");
        }




        

        // date and time and zone
        LocalDateTime currentTime = LocalDateTime.now();
        ZoneId uaZone = ZoneId.of("Europe/Kiev");
        ZoneId ruZone = ZoneId.of("Europe/Moscow");
        ZonedDateTime kiev = ZonedDateTime.of(currentTime, uaZone);
        System.out.println(kiev);
        ZonedDateTime rus = ZonedDateTime.of(currentTime, ruZone);
        System.out.println(rus);

  System.out.println("------------------------------------------");
        Instant instant = Instant.now();
        LocalDateTime localDateTime_1 = LocalDateTime.ofInstant(instant, ZoneId.of("Europe/Kiev"));
        LocalDateTime localDateTime_2 = LocalDateTime.ofInstant(instant, ZoneId.of("Europe/Moscow"));
        LocalDateTime localDateTime_3 = LocalDateTime.ofInstant(instant, ZoneId.of("America/New_York"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm:ss");
        System.out.println("The current date for Kiev: " + localDateTime_1.format(formatter));
        System.out.println("The current date for Moscow: " + localDateTime_2.format(formatter));
        System.out.println("The current date for NY: " + localDateTime_3.format(formatter));

        System.out.println("------------------------------------------");
        System.out.println(ZonedDateTime.of(localDateTime_1, ZoneId.of("Europe/Kiev")));
        System.out.println(ZonedDateTime.of(localDateTime_1, ZoneId.of("Europe/London")));
        System.out.println(ZonedDateTime.of(localDateTime_1, ZoneId.of("Europe/Madrid")));


    }
}
