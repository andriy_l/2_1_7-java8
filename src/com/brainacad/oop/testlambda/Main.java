package com.brainacad.oop.testlambda;

import java.util.*;
import java.util.stream.Stream;

/**
 * Created by andriy on 4/3/17.
 */
public class Main {
    public static void main(String[] args) {
        Integer[] integers = new Integer[10];
        Random random = new Random();
        for (int i = 0; i < integers.length; i++) {
            integers[i] = random.nextInt(10);
        }
        System.out.println("Source array " + Arrays.toString(integers));
//        Arrays.sort(integers, new Comparator<Integer>() {
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                return (o1 != o2 ? (o1 > o2 ? -1 : 1) : 0);
//            }
//        });
        Arrays.sort(integers, (o1, o2) ->  (o1 != o2 ? (o1 > o2 ? -1 : 1) : 0));
        System.out.println("Destination array " + Arrays.toString(integers));

        List<String> strings = Arrays
                .asList("one","two","bee","zoo","abc","defg","01","fff");
        System.out.println(strings);
        Collections.sort(strings, (s1, s2) -> (s2.compareToIgnoreCase(s1)));
        System.out.println(strings);


    }


}
