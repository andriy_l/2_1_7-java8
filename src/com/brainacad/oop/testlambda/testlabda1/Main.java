package com.brainacad.oop.testlambda.testlabda1;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7,8,9,10};
        List<String> strings = Arrays.asList("one","two","bee","zoo","abc","defg","01","fff", "after");

        System.out.println(sumEven(arr, new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return integer%2 == 0;
            }
        }));

        System.out.println(sumEven(arr, integer -> integer%2 == 0));

        String startLetters = "a";
        System.out.println(printJStr(strings,str -> str.startsWith(startLetters)));

        // 1-7-3
        updateList(strings);
        System.out.println(strings);
        strings.forEach(System.out::println);
        // equivalent to
        strings.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });
        // equivalent to
        strings.forEach(s -> System.out.println(s));
    }

    // парні
    public static long sumEven(int[] array, Predicate<Integer> integerPredicate){
        long result = 0;
        for (Integer i : array) {
          if(integerPredicate.test(i))
                result += i;
        }
        return result;
    }

    public static List<String> printJStr(List<String> stringList, Predicate<String> stringPredicate){
        List<String> strings = new ArrayList<>();
        for(String str : stringList) {
            if(stringPredicate.test(str)) {
                strings.add(str);
            }
        }
        return strings;
    }

    // 1-7-3
    public static void updateList(List<String> stringList){
        MyConverter myConverter = string -> {
            if(!MyConverter.isNull(string)){
                string = string.toUpperCase();
            }
            return string;
        };
        for(int i = 0; i < stringList.size(); i++ ) {
            stringList.set(i, myConverter.convertStr( stringList.get(i) ) );
        }

    }
}
