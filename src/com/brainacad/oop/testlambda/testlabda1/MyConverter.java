package com.brainacad.oop.testlambda.testlabda1;

@FunctionalInterface
public interface MyConverter {
    String convertStr(String string);

    public static boolean isNull(String string) {
        return string == null;
    }
    default void print(){};
    default void read(){};
}
