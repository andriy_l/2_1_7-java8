package com.brainacad.oop.teststreams;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by andriy on 4/4/17.
 */
public class TestStream {
    public static void main(String[] args) {

        Stream<Integer> integerStream = Stream.generate(() -> {int x = 0; return x+=10;}).limit(10).map(x -> x/2);
        integerStream.forEach(System.out::println);
//        List<Integer> integerList = integerStream.collect(Collectors.toList());
//        System.out.println(integerList);

        List<Integer> list = Stream.<Integer>iterate(10, i -> i+10).limit(10).map(x -> x/2).collect(Collectors.toList());
        System.out.println("List: " + list);



    }
}
