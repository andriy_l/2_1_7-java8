package samples.datetime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 * Created by andriy on 4/5/17.
 */
public class LTDemo {
    public static void main(String[] args) {
//        LocalDate date = LocalDate.now();
//        LocalTime time = LocalTime.now();
//        LocalDateTime dateTime = LocalDateTime.now();
//        ZoneId  zoneId = ZoneId.systemDefault();
//        ZonedDateTime zonedDateTime = ZonedDateTime.of(dateTime, zoneId);
//        long epochSeconds = 0;
//        Instant instant = zonedDateTime.toInstant();
//
//        long eS = instant.getEpochSecond();
//        Instant instant2 = Instant.ofEpochSecond(epochSeconds);
//        Instant instant3 = Instant.now();
//
//LocalDate date1 = LocalDate.of(2018, Month.APRIL, 40);
//
//System.out.println(date1.getYear() + "" + date1.getMonth() + "" + date1.getDayOfMonth());

        LocalDateTime d = LocalDateTime.of(2015, 5, 10, 11, 22, 33);

        Period p = Period.of(1, 2, 3);

        d = d.minus(p);

        DateTimeFormatter f = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);

        System.out.println(d.format(f));

    }
}
