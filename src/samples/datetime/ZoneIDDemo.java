package samples.datetime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;

/**
 * Created by andriy on 4/5/17.
 */
public class ZoneIDDemo {
    public static void main(String[] args) {
        String m1 = Duration.of(1, ChronoUnit.MINUTES).toString();

        String m2 = Duration.ofMinutes(1).toString();

        String s = Duration.of(60, ChronoUnit.SECONDS).toString();

        String d = Duration.ofDays(1).toString();
        System.out.println(d);

        String p = Period.ofDays(1).toString();
        System.out.println(p);

//        System.out.println(m1 == m2);
//        System.out.println(m1.equals(m2));
//        System.out.println(d == p);
        System.out.println(d.equals(p));
        System.out.println(m1.equals(m2));
    }
}
