package samples.functionalinterfaces;

/**
 * Created by andriy on 4/1/17.
 */
@FunctionalInterface
public interface MyFunctIfaceObjectDemo {
    void justDoIt();
    // default
    default void print(){};
    default void read(){};

    // inherited from Object
    String toString();
    int hashCode();
    boolean equals(Object o);

    // cannot override because it's final
    // Class<?> getClass();
    // void notify();
    // void notifyAll();
    // void wait();
    // void wait(long milliseconds);
}
