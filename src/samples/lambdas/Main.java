package samples.lambdas;

import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;


/**
 * Created by andriy on 4/1/17.
 */
public class Main {
    public static void main(String[] args) {

        Timer t = new Timer(1000, event ->
        {
            System.out.println("At the tone, the time is " + new Date());
            Toolkit.getDefaultToolkit().beep();
        });

        t.start();
        Timer t2 = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                            System.out.println("At the tone, the time is " + new Date());
            Toolkit.getDefaultToolkit().beep();
            }
        });
        int a = 3_000_000;
        int b = 0b010010100101010101;
        int c = 0433333333;
        int d = 0x4342FFFF;
        long z = 99999999999999L;
        double dd = 0.44E2;
        double dd2 = 0.44E-2;
        double dd3 = 0.1;

        System.out.println(Long.toBinaryString(z));
        System.out.println(Integer.toBinaryString(a));
        System.out.println(Double.toHexString(dd));
        System.out.println(Double.toHexString(dd3));





    }
}
