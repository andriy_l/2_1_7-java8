package samples.lambdas.food;

import java.util.Random;

/**
 * Created by andriy on 4/1/17.
 */
public class Animal {
    private FoodType[] validFood;
    public static final int NEEDED_CALLORIES = 3000;
    private int obtainedCallories;

    public Animal(FoodType[] foodType){
        this.validFood = foodType;
    }

    public void eating(Food food) {
        this.obtainedCallories
                += food.getCalories(validFood[new Random().nextInt(validFood.length)]);
    }

    public boolean isFull(){
        if(obtainedCallories >= NEEDED_CALLORIES){
            return true;
        } else {
            return false;
        }
    }
}
