package samples.lambdas.food;

@FunctionalInterface
public interface Food {

    int getCalories(FoodType foodType);

}
