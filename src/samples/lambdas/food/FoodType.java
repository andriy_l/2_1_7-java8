package samples.lambdas.food;

/**
 * Created by andriy on 4/1/17.
 */
public enum FoodType {
    APPLE(200), CARROT(400), MOUSE(200), MALONE(400), FISH(600);
    private int callories;
    private FoodType(int callories){
        this.callories = callories;
    }

    public int getCal(){
        return this.callories;
    }
}
