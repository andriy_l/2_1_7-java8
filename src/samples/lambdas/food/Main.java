package samples.lambdas.food;

import java.util.Random;

/**
 * Created by andriy on 4/1/17.
 */
public class Main {
    public static void main(String[] args) {
        FoodType[] validFoodPredator = {FoodType.MOUSE, FoodType.FISH};
        FoodType[] validFoodVegetarian = {FoodType.CARROT, FoodType.APPLE};

        Animal cat    = new Animal(validFoodPredator);
        while (!cat.isFull()){
            cat.eating(new Food() {
                @Override
                public int getCalories(FoodType foodType) {
                   FoodType food = validFoodPredator[new Random().nextInt(validFoodPredator.length)];
                    System.out.println("(CAT)I'm eating " + foodType);
                    return food.getCal();
                }
            });
        }

        Animal rabbit = new Animal(validFoodVegetarian);
        while (!rabbit.isFull()){
            rabbit.eating((FoodType foodType) ->  {
                    FoodType food = validFoodVegetarian[new Random().nextInt(validFoodVegetarian.length)];
                    System.out.println("(RABBIT)I'm eating " + foodType);
                    return food.getCal();
                });
        }

    }
}
