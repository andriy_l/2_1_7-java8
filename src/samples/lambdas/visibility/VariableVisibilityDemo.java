package samples.lambdas.visibility;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by andriy on 4/3/17.
 */
public class VariableVisibilityDemo {
    public static void main(String[] args) {
        repeatMessage("Hello", 1000);

    }

    public static void repeatMessage(String text, int delay) {
        //  код лямбда виразу може бути викликаний
        // через багато часу, коли змінних вже нема...
        // тобто значення змінної text може вже не існувати
        // Бо відкладене виконання!!!
        //
        // це так звані ВІЛЬНІ(free) змінні - змінні, які не є параметрами і не визначені в коді
        // у лямбда-виразах зберігаються вільні змінні
        // Тобто, ці значення ЗАХОПЛЕНІ lambda-виразом
        // формально блок коду з значеннями вільних змінних називається замиканням (closure)
        // В мові Java лямбда-вирази є замиканнями - closures
        ActionListener listener = event ->
        {
            System.out.println(text);
            Toolkit.getDefaultToolkit().beep();
        };

    }

    public static void countDown(int start, int delay)
    {
        ActionListener listener = event ->
        { // start must be final or effectively final
//            start--; // Error: Can't mutate captured variable
//            System.out.println(start);
        };
        new Timer(delay, listener).start();
    }

    public static void repeat(String text, int count)
    {
        for (int i = 1; i <= count; i++)
        {
            ActionListener listener = event ->
            {// i must be final or effectively final
//                System.out.println(i + ": " + text);
// Error: Cannot refer to changing i
            };
            new Timer(1000, listener).start();
        }
    }

    // RULE: кожна захоплена лямбда-виразом змінна має бути
    // effectively final or final
}
