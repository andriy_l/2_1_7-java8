package samples.lambdas.wrong;

/**
 * Created by andriy on 3/31/17.
 */
public class Human {
    public boolean haveAnimalFriend(Animal a){
        if(a.toString().equals("Dog") || a.toString().equals("Cat")){
            return true;
        }
        return false;
    }
}
