package samples.references;


import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Student{
    private String name;

    public Student(String name) {
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

public class ConstructorRefDemo {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Zenyk", "Vlodko", "Vladzo", "Bodyo", "Andriy", "Vasyl", "Petro");
        Stream<Student> studentStream = names.stream().map(Student::new); // calling constructor by reference
        // for every element of stream  like Person(String)
        List<Student> students = studentStream.collect(Collectors.toList()); // mutable reduction operation

        Collections.sort(names, String::compareToIgnoreCase);
        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
              return   o1.compareToIgnoreCase(o2);
            }
        });

    }

}
