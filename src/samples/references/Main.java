package samples.references;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        Timer t = new Timer(1000, event ->
        {
            System.out.println("At the tone, the time is " + new Date());
            Toolkit.getDefaultToolkit().beep();
        });

        Timer t2 = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                            System.out.println("At the tone, the time is " + new Date());
            Toolkit.getDefaultToolkit().beep();
            }
        });


        // references
        Timer t3 = new Timer(1000, event -> System.out.println(event));
        Timer t4 = new Timer (1000, System.out::println);

        // сортування стрічок без урахування регістру символів
        String[] strings = {"b", "d", "a", "o", "i", "z"};
                            // Comparable
        Arrays.sort(strings, String::compareToIgnoreCase);

        Arrays.sort(strings, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareToIgnoreCase(o2);
            }
        });

        // =====================================

    }
}
