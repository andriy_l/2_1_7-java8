package samples.references;


import java.util.ArrayList;
import java.util.List;

public class RefsSample {
    public static void main(String args[]){
        List<String> names = new ArrayList<>();

        names.add("Mahesh");
        names.add("Suresh");
        names.add("Ramesh");
        names.add("Naresh");
        names.add("Kalpesh");

        System.out.println("in j1.5: ");

        for (String s : names) {
            System.out.println(s);
        }


        System.out.println("in j1.8: ");
        names.forEach(System.out::println);
    }
}
