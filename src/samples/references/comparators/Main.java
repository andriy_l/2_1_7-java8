package samples.references.comparators;


import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Person {
    private String name;
    private String lastName;
    private String firstName;

    public String getName() {
        return name;
    }

    public Person(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}

public class Main {
    public static void main(String[] args) {
        Person[] people = {new Person("Ivan"), new Person("Andriy"), new Person("Vasyl")};
        // об'єкт :: методЦьогоОб'єкт
        Arrays.sort(people, Comparator.comparing(Person::getName));
        Arrays.sort(people, Comparator
                // Accepts a function that extracts a Comparable sort key from a type T,
                // and returns a Comparator<T> that compares by that sort key.
                .comparing(Person::getLastName)
                // Returns a comparator that imposes the reverse ordering of this comparator.
                .reversed()
                // Returns a lexicographic-order comparator with another comparator.
                .thenComparing(Person::getFirstName));
        List<String> names = Arrays.asList("Petro", "Dmytro");
        Stream<Person> stream = names.stream().map (Person::new) ;
        List<Person> peopleList = stream.collect(Collectors.toList());
    }
}
