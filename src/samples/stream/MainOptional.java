package samples.stream;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by andriy on 03.04.17.
 */
public class MainOptional {
    public static void main(String[] args) {
        List<String> people = Arrays.asList(new String("Ivan"), new String("Andriy"), new String("Vasyl"));
        // An Optional<T> object is a wrapper for either an object of type T or no object
        Optional<String> startsWithQ = people.stream().parallel().filter(s ->s.startsWith("Q")).findAny();
        System.out.println("word starts with Q: yes or no");
        boolean aWordStartsWithQ = people.stream().parallel().anyMatch(s -> s.startsWith("Q"));
    }
}
