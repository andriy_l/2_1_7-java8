package samples.stream;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * Created by andriy on 3/31/17.
 */
public class StreamCreationDemo2 {
    public static void main(String[] args) {

        Stream<String> streamOfStringArray = Stream.of("one","two","three");

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("one"); arrayList.add("two"); arrayList.add("three");
        Stream<String> streamOfCollection = arrayList.stream();

        // infinite streams
        Stream<String> infiniteStream = Stream.generate(() -> " infinity "); // infinite samples.stream of ininity
        Stream<Double> infiniteDoubleStream = Stream.generate(Math::random); // infinite samples.stream of random values

        // infinite sequence for iteration
                                                        // seed             (i)  new UnaryOperator<T>() { T.add(T_previous)}
        Stream<BigInteger> integerStream = Stream.iterate(BigInteger.ZERO, n -> n.add(BigInteger.ONE));


    }
}
