package samples.stream;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Created by andriy on 3/30/17.
 */
public class StreamDemo1 {
    public static void main(String[] args) {
        String contents = null;
        try {
            contents = new String(Files.readAllBytes(Paths.get("text","Khiba_revut_voly.txt")), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Read file into string
        List<String> words = Arrays.asList(contents.split("\\PL+"));
        // Split into words; nonletters are delimiters
        long count = 0;
        for (String w : words)
        {
            if (w.length() > 12) count++;
        }
        System.out.println(" c1: " + count);

        // samples.stream version
        long count2 = words.stream()       //.parallel()
                .filter((String w) -> {return (w.length() > 12);})
                .count();
        System.out.println(" c2: " + count2);

        // samples.stream version
        long count3 = words.parallelStream()
                .filter(w -> w.length() > 12)
                .count();
        System.out.println(" c3: " + count3);
    }
}
