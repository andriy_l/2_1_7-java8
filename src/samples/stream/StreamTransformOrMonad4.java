package samples.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamTransformOrMonad4 {
    public static void main(String[] args) {
        // samples.stream as array
        System.out.println(Arrays.toString(letters("java").toArray()));
        // samples.stream of streams
        List<String> words = Arrays.asList("JAVA", "HASKELL", "OCAML", "LISP");
        Stream<Stream<String>> result = words.stream().map(w -> letters(w));
        // You will get a samples.stream of streams, like [. . . ["y", "o", "u", "r"], ["b", "o", "a", "t"], . . .].
        // To flatten it out to a samples.stream of letters [. . ."y", "o", "u", "r", "b", "o", "a", "t", . . .],
        // use the flatMap method instead of map:
        Stream<String> flatResult = words.stream().flatMap(w -> letters(w));
        System.out.println(Arrays.toString(flatResult.toArray()));
        /*
You will find a flatMap method in classes other than streams. It is a general concept in
computer science. Suppose you have a generic type G (such as Stream) and functions f
from some type T to G<U> and g from U to G<V>. Then you can compose them—that is,
first apply f and then g, by using flatMap. This is a key idea in the theory of monads.
But don’t worry—you can use flatMap without knowing anything about monads.
         */
    }

    public static Stream<String> letters(String s) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < s.length(); i++)
            result.add(s.substring(i, i + 1));
        return result.stream();
    }
}
