package samples.stream.testdemo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by andriy on 4/10/17.
 */
public class ListStreamTest {
    private static List<String> sort(List<String> list) {
        List<String> copy = new ArrayList<>(list);
        Collections.sort(copy, (a, b) -> b.compareTo(a));
//        return copy;
        return list.stream()

                . sorted((a, b) -> a. compareTo (b))

                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
//        List<String> list = Arrays.asList("A", "Z", "C", "E");
//        System.out.println(sort(list));
        Predicate<? super String> predicate = s -> s.startsWith("g");
        Stream<String>stream1 = Stream.generate(() -> "growl!");
        Stream<String>stream2 = Stream.generate(() -> "growl!");
        boolean b1 = stream1.anyMatch (predicate);
        boolean b2 = stream2.anyMatch (predicate);
        System.out.println (b1 + " " + b2);

    }
}
