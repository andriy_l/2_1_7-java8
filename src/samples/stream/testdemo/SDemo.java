package samples.stream.testdemo;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by andriy on 4/5/17.
 */
public class SDemo {
    public static void main(String[] args) {
        Map<String, String> props = new HashMap<>();
        props.put("key1", "value1");
        props.put("key2", "value2");
        props.put("key3", "value3");
        props.put("key4", "value4");
        props.put("key5", "value5");

//        props.keySet().stream().map(k -> props.get(k)).forEach(System.out::println);
//
////        Stream.generate (() -> "1").limit(10).forEach(System.out::println);
////        System.out.println();
////        Stream.generate (() -> "1").filter(x -> x.length()>0).limit(10).forEach(System.out::println);
////        System.out.println();
////        Stream.generate (() -> "1").limit(10).peek(System.out::println).forEach(System.out::println);
////        System.out.println();
////
////        Stream.generate (() -> "1").limit(10).peek(System.out::println).forEach(System.out::println);
//        Stream.generate (() -> "1").filter(x -> x.length() > 0).limit(10).forEach(System.out::println);

//        Stream<String> s = Stream.empty();
//        Stream<String> s2 = Stream.empty();
//        Map<Boolean, List<String>>  p = s.collect(Collectors.partitioningBy (b -> b.startsWith("c")));
//        Map<Boolean, List<String>>  g = s2.collect(Collectors.groupingBy (b -> b.startsWith("c")));
//
//        System.out.println (p + "" + g);

        Stream.generate (() -> "1").filter(x -> x.length() > 0).limit(10).peek(System.out::println).forEach(System.out::println);

    }
}
