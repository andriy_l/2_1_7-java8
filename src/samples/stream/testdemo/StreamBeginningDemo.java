package samples.stream.testdemo;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.DoubleConsumer;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

/**
 * Created by andriy on 05.04.17.
 */
public class StreamBeginningDemo {
    public static void main(String[] args) {
        List<String> list1 = Arrays.asList("Four", "Five", "Tank", "Taras");
        List<String> list2 = Arrays.asList("Six", "Seven", "Ship");
        Stream<String> stringStream = Stream.of("One", "Two", "Three", "Tree");

        Stream<String> stringStream2    =
                Stream
                // створити потік
                .concat(list1.stream(),list2.stream())
                // проміжні операції
                // 1 фільтрації
                .filter(s -> s.startsWith("T"))
                .peek(System.out::println)
                .filter(s -> s.length() > 3) // predicate
                // 2 map
                .peek(System.out::println)
                .map(s -> s.substring(0,2)) // Function
                .peek(System.out::println) // consumer
                .map(String::toUpperCase)
                .peek(System.out::println)
                .distinct() // unique data
                        .sorted()
                .limit(5)
                .skip(3);
//                .forEachOrdered(System.out::println);
//                .forEach(System.out::println);

        // термінальні операції
       List<String> result = stringStream2
        .collect(Collectors.toList()); // колекціонуємо дані

        Set<String> stringsR = stringStream2.collect(Collectors.toSet());

        System.out.println(result);
        long count = stringStream2.count();


//        DoubleStream doubleStream = new Random()
//                .doubles().forEach(new DoubleConsumer() {
//                    @Override
//                    public void accept(double value) {
//                        System.out.println(value);
//                    }
//                });
        Stream<String> emptyStream = Stream.empty();


        // unbounded stream - нескінченний потік
                Stream.generate(() -> new Random().nextInt(100))
                        .filter(i -> (i > 10))
                        .filter(i -> (i< 90))
                .forEach(System.out::println);

//        Stream.generate(Math::random).peek(x -> {
//            if(x > 0.7) {
//                System.out.println("gt0.7 "+x);
//            }
//        })
//                .forEach(System.out::println);
//        System.out.println(integerList);
//        System.out.println(integerList.size());



    }
}
